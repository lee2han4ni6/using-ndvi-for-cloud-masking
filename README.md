# Using NDVI for Cloud Masking 

This code shows the use of NDVI to create cloud mask, using NDVI>=0.33 which signifies very healthy plants. However, the conclusion reached was that using NDVI as cloud mask was quite accurate for maximum temperatures, but not for minimuum temperatures.
